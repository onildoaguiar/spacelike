﻿using UnityEngine;
using UnityEngine.UI;

public class CooldownBarControl : MonoBehaviour
{

    public int shootTime;
    public Image visualShootTime;
    private int currentShootIime = 100;
    private float countdownShootTime;

    public int shieldTime;
    public Image visualShieldTime;
    private int currentShieldTime = 100;
    private float countdownShieldTime;

    public int skillTime;
    public Image visualSkillTime;
    private int currentSkillTime = 0;
    private float countdownSkillTime;

    private byte timeToColor;
    private int skillModifier = 1;

    // Use this for initialization
    void Start()
    {
        countdownShootTime = shootTime;
        countdownShieldTime = shieldTime;

        countdownSkillTime = Utils.GetSkillTime();
        visualSkillTime.fillAmount = (countdownSkillTime / skillTime);
    }

    public bool FinishedShootTime(bool isShoot)
    {

        if (isShoot)
        {
            countdownShootTime -= Time.deltaTime;
            visualShootTime.fillAmount -= 1.0f / shootTime * Time.deltaTime;
        }
        else if (countdownShootTime < 100)
        {
            countdownShootTime += (Time.deltaTime / 1.5f);
            visualShootTime.fillAmount += 1.0f / shootTime * (Time.deltaTime / 1.5f);
        }

        currentShootIime = (int)(visualShootTime.fillAmount * 100);

        if (countdownShootTime < 0)
        {
            return true;
        }

        return false;
    }

    public bool FinishedShieldTime(bool isShield)
    {
        if (isShield)
        {
            countdownShieldTime -= Time.deltaTime;
            visualShieldTime.fillAmount -= 1.0f / shieldTime * Time.deltaTime;
        }
        else if (currentShieldTime < 100) {
            countdownShieldTime += (Time.deltaTime / 4.0f);
            visualShieldTime.fillAmount += 1.0f / shieldTime * (Time.deltaTime / 4.0f);
        }

        currentShieldTime = (int)(visualShieldTime.fillAmount * 100);

        if (countdownShieldTime < 0)
        {
            return true;
        }

        return false;
    }

    public bool FinishedSkillTime(bool isShield, bool isEnemyShoot)
    {
        if (isShield && isEnemyShoot)
        {
            countdownSkillTime += (Time.deltaTime / skillModifier);
            visualSkillTime.fillAmount += 1.0f / skillTime * (Time.deltaTime / skillModifier);
        }

        currentSkillTime = (int)(visualSkillTime.fillAmount * 100);

        if (countdownSkillTime >= skillTime)
        {
            return true;
        }

        return false;
    }

    byte ConvertToColor(int currentTime)
    {

        if (currentTime > 50)
        {
            timeToColor = (byte)(0 - ((currentTime * 255) / 50));
        }
        else
        {
            timeToColor = (byte)((currentTime * 255) / 50);

        }
        return timeToColor;
    }

    public void ResetCountdowns()
    {
        countdownShootTime = shootTime;
        visualShootTime.fillAmount = 1f;
        currentShootIime = 100;

        countdownShieldTime = shieldTime;
        visualShieldTime.fillAmount = 1f;
        currentShieldTime = 100;

        countdownSkillTime = skillTime;
        visualSkillTime.fillAmount = 0f;
        currentSkillTime = 0;
    }

    public void AddPlusPreviewTime()
    {
        countdownShootTime = shootTime;
        visualShootTime.fillAmount = 1f;

    }

    public void ResetSkillTime() {
        countdownSkillTime = 0;
        visualSkillTime.fillAmount = 0f;
        currentSkillTime = 0;
    }

    public void AddPlusPlayTime()
    {
        countdownShieldTime = shieldTime;
        visualShieldTime.fillAmount = 1f;
    }

    public int getCurrentShootTime()
    {
        return currentShootIime;
    }

    public int getCurrentShieldTime()
    {
        return currentShieldTime; ;
    }

    public float getCurrentSkillTime()
    {
        return countdownSkillTime;
    }
}
