﻿using UnityEngine;
using UnityEngine.UI;

public class LifeBarControl : MonoBehaviour
{

    public int playerLife;
    public Image visualPlayerLife;
    private int currentPlayerLife;
    private float countdownPlayerLife;

    public int enemyLife;
    public Image visualEnemyLife;
    private int currentEnemeyLife = 100;
    private float countdownEnemyLife;

    private byte timeToColor;
    private int emptyModifier = 2;

    // Use this for initialization
    void Start()
    {
        countdownPlayerLife = Utils.GetPlayerLife();
        currentPlayerLife = Utils.GetPercentualPlayerLife();
        visualPlayerLife.fillAmount = (countdownPlayerLife / playerLife);
   
        countdownEnemyLife = enemyLife;

        visualPlayerLife.color = new Color32(ConvertToColor(currentPlayerLife), 255, 0, 255);
        visualEnemyLife.color = new Color32(ConvertToColor(currentEnemeyLife), 255, 0, 255);
    }

    public bool DefeatedPlayer(bool isEnemyShoot, bool isPlayerShield, bool isPlayerShoot, int enemyPower)
    {

        if (isEnemyShoot && !isPlayerShield)
        {
            if (isPlayerShoot)
            {
                countdownPlayerLife -= enemyPower;
                visualPlayerLife.fillAmount -= 1.0f / playerLife * enemyPower;
            }
            else
            {
                countdownPlayerLife -= (enemyPower * emptyModifier);
                visualPlayerLife.fillAmount -= 1.0f / playerLife * (enemyPower * emptyModifier);
            }
        }

        currentPlayerLife = (int)(visualPlayerLife.fillAmount * 100);


        if (currentPlayerLife > 50)
        {
            visualPlayerLife.color = new Color32(ConvertToColor(currentPlayerLife), 255, 0, 255);
        }
        else
        {
            visualPlayerLife.color = new Color32(255, ConvertToColor(currentPlayerLife), 0, 255);
        }

        if (countdownPlayerLife < 0)
        {
            return true;
        }

        return false;
    }

    public bool DefeatedEnemy(bool isPlayerShoot, bool isEnemyShield, bool isEnemyEmpty, int playerPower)
    {
        if (isPlayerShoot && !isEnemyShield)
        {
            if (!isEnemyEmpty)
            {
                countdownEnemyLife -= playerPower;
                visualEnemyLife.fillAmount -= 1.0f / enemyLife * playerPower;
            }
            else
            {
                countdownEnemyLife -= (playerPower * emptyModifier);
                visualEnemyLife.fillAmount -= 1.0f / enemyLife * (playerPower * emptyModifier);
            }

        }

        currentEnemeyLife = (int)(visualEnemyLife.fillAmount * 100);


        if (currentEnemeyLife > 50)
        {
            visualEnemyLife.color = new Color32(ConvertToColor(currentEnemeyLife), 255, 0, 255);
        }
        else
        {
            visualEnemyLife.color = new Color32(255, ConvertToColor(currentEnemeyLife), 0, 255);
        }

        if (countdownEnemyLife < 0)
        {
            return true;
        }

        return false;
    }

    byte ConvertToColor(int currentTime)
    {

        if (currentTime > 50)
        {
            timeToColor = (byte)(0 - ((currentTime * 255) / 50));
        }
        else
        {
            timeToColor = (byte)((currentTime * 255) / 50);

        }
        return timeToColor;
    }

    public void ResetCountdowns()
    {
        countdownPlayerLife = playerLife;
        visualPlayerLife.fillAmount = 1f;
        currentPlayerLife = 100;
        visualPlayerLife.color = new Color32(ConvertToColor(currentPlayerLife), 255, 0, 255);

        countdownEnemyLife = enemyLife;
        visualEnemyLife.fillAmount = 1f;
        currentEnemeyLife = 100;
        visualEnemyLife.color = new Color32(ConvertToColor(currentEnemeyLife), 255, 0, 255);
    }

    public void AddPlusPreviewTime()
    {
        countdownPlayerLife = playerLife;
        visualPlayerLife.fillAmount = 1f;

    }

    public void AddPlusPlayTime()
    {
        countdownEnemyLife = enemyLife;
        visualEnemyLife.fillAmount = 1f;
    }

    public void EnemyDamage(float damage) {
        countdownEnemyLife -= damage;
        visualEnemyLife.fillAmount -= 1.0f / enemyLife * damage;
    }

    public float getCurrentPlayerLife()
    {
        return countdownPlayerLife;
    }

    public int getPercentualPlayerLife()
    {
        return currentPlayerLife;
    }

    public float getCurrentLEnemyLife()
    {
        return countdownEnemyLife; ;
    }
}
