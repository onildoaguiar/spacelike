﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyBehaviour : MonoBehaviour
{

    public float shootTime;
    private float countdownShootTime;

    public float shieldTime;
    private float countdownShieldTime;

    public float emptyTime;
    private float countdownEmptyTime;

    // Use this for initialization
    void Start()
    {
        countdownShootTime = shootTime;
        countdownShieldTime = shieldTime;
        countdownEmptyTime = emptyTime;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public bool FinishedShootTime()
    {
        countdownShootTime -= Time.deltaTime;

        if (countdownShootTime < 0)
        {
            ResetShootTime();
            return true;
        }

        return false;
    }

    public bool FinishedShieldTime()
    {
        countdownShieldTime -= Time.deltaTime;

        if (countdownShieldTime < 0)
        {
            ResetShieldTime();
            return true;
        }

        return false;
    }

    public bool FinishedEmptyTime()
    {
        countdownEmptyTime -= Time.deltaTime;

        if (countdownEmptyTime < 0)
        {
            ResetEmptyTime();
            return true;
        }

        return false;
    }

    public void ResetCountdowns()
    {
        countdownShootTime = shootTime;
        countdownShieldTime = shieldTime;
    }

    public void ResetShootTime()
    {
        countdownShootTime = shootTime;
    }

    public void ResetShieldTime()
    {
        countdownShieldTime = shieldTime;
    }

    public void ResetEmptyTime()
    {
        countdownEmptyTime = emptyTime;
    }
}
