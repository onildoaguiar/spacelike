using UnityEngine;

public class StateMachineBehaviour : MonoBehaviour {

	public enum StateMachine {
		PRE_START,
		START,
		RESTART,
		RELOAD,
		PAUSED,
		PLAY,
		WIN,
		PRE_LOSE,
		LOSE,
		TUTORIAL,
        CONTINUE,
        NULL
	}
}
