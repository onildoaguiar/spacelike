﻿using UnityEngine;

public static class Utils
{

    public static void SetFinishGame(int finish)
    {
        PlayerPrefs.SetInt("finishGame", finish);
    }

    public static int GetFinishGame()
    {
        return PlayerPrefs.GetInt("finishGame", 0);
    }

    public static void SetEnemyAction(string action)
    {
        PlayerPrefs.SetString("enemyAction", action);
    }

    public static string GetEnemyAction()
    {
        return PlayerPrefs.GetString("enemyAction", "empty");
    }

    public static void SetStarsAmount(int stars)
    {
        int starsAmount = GetStarsAmount();
        starsAmount += stars;
        PlayerPrefs.SetInt("starsAmount", starsAmount);
    }

    public static int GetStarsAmount()
    {
        return PlayerPrefs.GetInt("starsAmount", 0);
    }

    public static int GetStars()
    {
        return PlayerPrefs.GetInt("stars", 0);
    }

    public static void SetStars(int stars)
    {
        int starsRun = GetStars();
        starsRun += stars;
        PlayerPrefs.SetInt("stars", starsRun);
    }

    public static void SetKills()
    {
        int kills = GetKills();
        kills++;
        PlayerPrefs.SetInt("kills", kills);
    }

    public static int GetKills()
    {
        return PlayerPrefs.GetInt("kills", 0);
    }

    public static void SetLevel(string level)
    {
        PlayerPrefs.SetString("level", level);
    }

    public static string GetLevel()
    {
        return PlayerPrefs.GetString("level", "Space-1");
    }

    public static void SetPlayerLife(float playerLife)
    {
        PlayerPrefs.SetFloat("playerLife", playerLife);
    }

    public static float GetPlayerLife()
    {
        return PlayerPrefs.GetFloat("playerLife", 3000);
    }

    public static void SetPercentualPlayerLife(int playerLife)
    {
        PlayerPrefs.SetInt("percentualPlayerLife", playerLife);
    }

    public static int GetPercentualPlayerLife()
    {
        return PlayerPrefs.GetInt("percentualPlayerLife", 100);
    }

    public static void SetSkillTime(float skillTime)
    {
        PlayerPrefs.SetFloat("skillTime", skillTime);
    }

    public static float GetSkillTime()
    {
        return PlayerPrefs.GetFloat("skillTime", 0);
    }

    public static void ResetInfos()
    {
        SetPlayerLife(3000);
        SetPercentualPlayerLife(100);
        SetSkillTime(0);
        PlayerPrefs.SetInt("kills", 0);
        PlayerPrefs.SetInt("stars", 0);
        SetLevel("Space-1");
        SetFinishGame(0);
    }


}
