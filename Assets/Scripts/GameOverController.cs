﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour
{
    public Text level;
    public Text kills;
    public Text stars;
    public Text starsAmount;
    public Text gameOver;
    public GameObject medal;

    // Use this for initialization
    void Start()
    {
        level.text = "Level: " + Utils.GetLevel();
        kills.text = "Kills: " + Utils.GetKills();
        stars.text = "Stars: " + Utils.GetStars();
        starsAmount.text = "" + Utils.GetStarsAmount();
        medal.SetActive(false);

        if (Utils.GetFinishGame().Equals(1))
        {
            gameOver.text = "YOU ARE SPECIAL";
            medal.SetActive(true);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Play()
    {
        SceneManager.LoadScene("Loading");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
