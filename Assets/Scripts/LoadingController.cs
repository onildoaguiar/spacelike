﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("PlayAgain");
    }

    IEnumerator PlayAgain()
    {
        yield return new WaitForSeconds(0.5f);

        Utils.ResetInfos();

        SceneManager.LoadScene("Space-1");       
    }
}
