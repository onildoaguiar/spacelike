﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject playerShoot, playerShield, enemyShoot, enemyShield, enemySkill;
    public Animator player, enemy;
    public StateMachineBehaviour.StateMachine currentState;

    public CooldownBarControl playerBehaviour;
    public LifeBarControl lifeBarControl;
    public EnemyBehaviour enemyBehaviour;
    public GameObject pauseMenu;
    public GameObject skillButton;
    public Text level;
    public Text starsAmount;
    public int starsValue;


    private StateMachineBehaviour.StateMachine lastState;
    private bool shootButtonOn, shieldButtonOn;
    private bool checkPlayerShootTime, checkPlayerShieldTime, checkPlayerSkillTime;
    private bool checkPlayerLife, checkEnemyLife;
    private bool isPlayerShoot, isPlayerShield;
    private bool isEnemyShoot, isEnemyShield, isEnemyEmpty;
    private bool isRunning = false;

    // Use this for initialization
    void Start()
    {
        playerBehaviour = FindObjectOfType<CooldownBarControl>();
        lifeBarControl = FindObjectOfType<LifeBarControl>();
        enemyBehaviour = FindObjectOfType<EnemyBehaviour>();

        currentState = StateMachineBehaviour.StateMachine.PRE_START;
        lastState = StateMachineBehaviour.StateMachine.NULL;
        playerShoot.SetActive(false);
        playerShield.SetActive(false);
        enemyShoot.SetActive(false);
        enemyShield.SetActive(false);
        enemySkill.SetActive(false);
        pauseMenu.SetActive(false);
        skillButton.SetActive(false);
        isPlayerShoot = false;
        isPlayerShield = false;
        isEnemyShoot = false;
        isEnemyShield = false;
        isEnemyEmpty = true;
        shootButtonOn = false;
        shieldButtonOn = false;

        Utils.SetLevel(level.text);
    }

    // Update is called once per frame
    void Update()
    {
        starsAmount.text = "" + Utils.GetStarsAmount();
        GameStateMachine();
    }

    public void SwitchState(StateMachineBehaviour.StateMachine nextState)
    {
        lastState = currentState;
        currentState = nextState;
    }

    public StateMachineBehaviour.StateMachine getCurrentStateMachine()
    {
        return this.currentState;
    }

    public StateMachineBehaviour.StateMachine getLastStateMachine()
    {
        return this.lastState;
    }

    void GameStateMachine()
    {

        switch (currentState)
        {

            case StateMachineBehaviour.StateMachine.PRE_START:
                {
                    EnemyShield();
                    SwitchState(StateMachineBehaviour.StateMachine.START);
                    break;
                }

            case StateMachineBehaviour.StateMachine.START:
                {
                    SwitchState(StateMachineBehaviour.StateMachine.PLAY);
                    break;
                }

            case StateMachineBehaviour.StateMachine.RESTART:
                {
                    break;
                }

            case StateMachineBehaviour.StateMachine.PAUSED:
                {
                    break;
                }

            case StateMachineBehaviour.StateMachine.PLAY:
                {
                    checkPlayerShootTime = playerBehaviour.FinishedShootTime(isPlayerShoot);
                    checkPlayerShieldTime = playerBehaviour.FinishedShieldTime(isPlayerShield);
                    checkPlayerSkillTime = playerBehaviour.FinishedSkillTime(isPlayerShield, isEnemyShoot);
                    checkPlayerLife = lifeBarControl.DefeatedPlayer(isEnemyShoot, isPlayerShield, isPlayerShoot, 5);
                    checkEnemyLife = lifeBarControl.DefeatedEnemy(isPlayerShoot, isEnemyShield, isEnemyEmpty, 5);

                    if (checkPlayerSkillTime)
                    {
                        skillButton.SetActive(true);
                    }
                    else
                    {
                        skillButton.SetActive(false);
                    }

                    if (isPlayerShoot && !isEnemyShield && !checkEnemyLife)
                    {
                        enemy.SetBool("IsDamage", true);
                    }
                    else
                    {
                        enemy.SetBool("IsDamage", false);
                    }

                    if (isEnemyShoot && !isPlayerShield && !checkPlayerLife)
                    {
                        player.SetBool("IsDamage", true);
                    }
                    else
                    {
                        player.SetBool("IsDamage", false);
                    }

                    // Enemy Behaviour
                    EnemyAI();

                    if (shieldButtonOn && playerBehaviour.getCurrentShieldTime() >= 50)
                    {
                        PlayerShield();
                    }

                    if (shootButtonOn && playerBehaviour.getCurrentShootTime() >= 50)
                    {
                        PlayerShoot();
                    }

                    if (checkPlayerShootTime)
                    {
                        CancelPlayerShoot();
                        isPlayerShoot = false;
                    }

                    if (checkPlayerShieldTime)
                    {
                        CancelPlayerShield();
                        isPlayerShield = false;
                    }

                    if (checkPlayerLife && !checkEnemyLife && !isRunning)
                    {
                        isRunning = true;
                        player.SetBool("IsDefeat", true);
                        enemy.enabled = false;
                        CleanDefeat();
                        isRunning = false;
                        SwitchState(StateMachineBehaviour.StateMachine.LOSE);
                    }

                    if (checkEnemyLife && !checkPlayerLife && !isRunning)
                    {
                        isRunning = true;
                        enemy.SetBool("IsDefeat", true);
                        player.enabled = false;
                        CleanDefeat();
                        isRunning = false;

                        int stars = lifeBarControl.getPercentualPlayerLife() * starsValue;
                        Utils.SetKills();
                        Utils.SetStars(stars);
                        Utils.SetStarsAmount(stars);
                        Utils.SetPlayerLife(lifeBarControl.getCurrentPlayerLife());
                        Utils.SetSkillTime(playerBehaviour.getCurrentSkillTime());
                        Utils.SetPercentualPlayerLife(lifeBarControl.getPercentualPlayerLife());

                        SwitchState(StateMachineBehaviour.StateMachine.WIN);
                    }

                    break;
                }

            case StateMachineBehaviour.StateMachine.WIN:
                {
                    if (!isRunning) StartCoroutine("WaitSecondsWin");
                    break;
                }

            case StateMachineBehaviour.StateMachine.LOSE:
                {
                    if (!isRunning) StartCoroutine("WaitSecondsLose");
                    break;
                }
        }
    }

    public void KeyDown(GameObject action)
    {
        if ("playerShoot".Equals(action.tag) && !isPlayerShield)
        {
            shootButtonOn = true;

            // Camcel Shield
            CancelPlayerShield();

            // Active Shoot
            PlayerShoot();

        }

        if ("playerShield".Equals(action.tag) && !isPlayerShoot)
        {
            shieldButtonOn = true;

            // Camcel Shoot
            CancelPlayerShoot();

            // Active Shield
            PlayerShield();
        }
    }

    public void KeyUp(GameObject action)
    {
        if ("playerShoot".Equals(action.tag))
        {
            shootButtonOn = false;
            isPlayerShoot = false;

            // Camcel Shoot
            CancelPlayerShoot();
        }

        if ("playerShield".Equals(action.tag))
        {
            shieldButtonOn = false;
            isPlayerShield = false;

            // Cancel Shield
            CancelPlayerShield();
        }
    }

    private void PlayerShoot()
    {
        if (playerBehaviour.getCurrentShootTime() >= 50)
        {
            // Active Shoot
            player.SetBool("IsShoot", true);
            playerShoot.SetActive(true);
            isPlayerShoot = true;
        }
    }

    public void PlayerShield()
    {
        if (playerBehaviour.getCurrentShieldTime() >= 50)
        {
            // Active Shield
            player.SetBool("IsShield", true);
            playerShield.SetActive(true);
            isPlayerShield = true;
        }
    }

    private void CancelPlayerShoot()
    {
        // Camcel Shoot
        player.SetBool("IsShoot", false);
        playerShoot.SetActive(false);
        isPlayerShoot = false;
    }

    private void CancelPlayerShield()
    {
        // Cancel Shield
        player.SetBool("IsShield", false);
        playerShield.SetActive(false);
        isPlayerShield = false;
    }

    private void EnemyShoot()
    {
        // Cancel Enemy Shield
        CancelEnemyShield();

        // Active Enemy Shoot
        enemy.SetBool("IsShoot", true);
        enemyShoot.SetActive(true);
        isEnemyShoot = true;
        Utils.SetEnemyAction("shoot");
    }

    private void CancelEnemyShoot()
    {
        // Camcel Enemy Shoot
        enemy.SetBool("IsShoot", false);
        enemyShoot.SetActive(false);
        isEnemyShoot = false;
        isEnemyEmpty = false;
    }

    private void EnemyShield()
    {
        // Camcel Enemy Shoot
        CancelEnemyShoot();

        // Active Enemy Shield
        enemy.SetBool("IsShield", true);
        enemyShield.SetActive(true);
        isEnemyShield = true;
        Utils.SetEnemyAction("shield");
    }

    private void CancelEnemyShield()
    {
        // Cancel Enemy Shield
        enemy.SetBool("IsShield", false);
        enemyShield.SetActive(false);
        isEnemyShield = false;
        isEnemyEmpty = false;
    }


    private void EnemyEmpty()
    {
        // Camcel Enemy Shoot and Enemy Shield
        CancelEnemyShoot();
        CancelEnemyShield();

        // Active Enemy Empty
        isEnemyEmpty = true;
        Utils.SetEnemyAction("empty");
    }

    private void EnemyAI()
    {

        switch (Utils.GetEnemyAction())
        {

            case "empty":
                {
                    if (enemyBehaviour.FinishedEmptyTime())
                    {
                        if (Random.Range(1, 10) == 2)
                        {
                            EnemyShoot();
                        }
                        else
                        {
                            EnemyShield();
                        }

                    }
                    break;
                }

            case "shield":
                {
                    if (enemyBehaviour.FinishedShieldTime())
                    {
                        if (Random.Range(1, 10) == 2)
                        {
                            EnemyEmpty();
                        }
                        else
                        {
                            EnemyShoot();
                        }
                    }
                    break;
                }

            case "shoot":
                {
                    if (enemyBehaviour.FinishedShootTime())
                    {
                        if (Random.Range(1, 10) == 2)
                        {
                            EnemyShield();
                        }
                        else
                        {
                            EnemyEmpty();
                        }
                    }
                    break;
                }

            default:
                if (enemyBehaviour.FinishedEmptyTime())
                {
                    if (Random.Range(1, 2) == 1)
                    {
                        EnemyShield();
                    }
                    else
                    {
                        EnemyShoot();
                    }

                }
                break;
        }
    }

    IEnumerator WaitSecondsLose()
    {
        isRunning = true;
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("GameOver");
        isRunning = false;
    }

    IEnumerator WaitSecondsWin()
    {
        isRunning = true;
        yield return new WaitForSeconds(1.0f);

        switch (Utils.GetLevel())
        {

            case "Space-1":
                {
                    SceneManager.LoadScene("Space-2");
                    break;
                }

            case "Space-2":
                {
                    SceneManager.LoadScene("Space-3");
                    break;
                }

            case "Space-3":
                {
                    Utils.SetFinishGame(1);
                    SceneManager.LoadScene("GameOver");
                    break;
                }

            default:
                break;
        }
        isRunning = false;
    }

    IEnumerator WaitEnemySkill()
    {
        isRunning = true;
        yield return new WaitForSeconds(0.5f);
        enemy.SetBool("IsSkill", false);
        enemySkill.SetActive(false);
        isRunning = false;
    }

    public void PauseMenu()
    {
        pauseMenu.SetActive(true);
        StopAnimations();
        SwitchState(StateMachineBehaviour.StateMachine.PAUSED);
    }

    public void Continue()
    {
        pauseMenu.SetActive(false);
        StartAnimations();
        SwitchState(StateMachineBehaviour.StateMachine.PLAY);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Loading");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Skill()
    {
        lifeBarControl.EnemyDamage(500);
        enemy.SetBool("IsSkill", true);
        enemySkill.SetActive(true);
        playerBehaviour.ResetSkillTime();
        if (!isRunning) StartCoroutine("WaitEnemySkill");
    }

    public void StopAnimations()
    {
        player.enabled = false;
        enemy.enabled = false;
    }
    public void StartAnimations()
    {
        player.enabled = true;
        enemy.enabled = true;
    }

    public void CleanDefeat()
    {
        playerShoot.SetActive(false);
        playerShield.SetActive(false);
        enemyShoot.SetActive(false);
        enemyShield.SetActive(false);
        enemySkill.SetActive(false);
    }
}
